package AES;

import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.*;

import javax.swing.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;


public class Synonims {

    public void getSynonims(String wordd, JTextArea area, String pos) {
        Synonims word = new Synonims();
        if (pos.equals("NN") || pos.equals("NNS") || pos.equals("NNP") || pos.equals("NNPS") || pos.equals("PDT")
                || pos.equals("POS") || pos.equals("PRP") || pos.equals("PRP$") || pos.equals("EX") || pos.equals("FW")
                || pos.equals("JJ") || pos.equals("JJR") || pos.equals("JJS") || pos.equals("LS") || pos.equals("MD")
                || pos.equals("RB") || pos.equals("RBR") || pos.equals("RBS")) {
            word.askSynoNoun(wordd, area);
        } else if (pos.equals("VB") || pos.equals("VBD")
                || pos.equals("VBG") || pos.equals("VBN")) {
            try {
                word.askSynoVerb(wordd, area);
            } catch (Exception e) {
                System.out.println(pos + " " + wordd);
            }

        }
    }

    public void askSynoNoun(String wordd, JTextArea area) {
		 	/*String wordNetDirectory = "WordNet-3.0";
	        String path = wordNetDirectory + File.separator + "dict";*/
        URL url;
        IDictionary dict = null;
        try {
            url = new URL("file", null, "C:\\Users\\Hasaka\\workspace\\AES\\dict");
            dict = new Dictionary(url);
            try {
                dict.open();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            IIndexWord idxWord = dict.getIndexWord(wordd, POS.NOUN);
            IWordID wordID = idxWord.getWordIDs().get(0);
            IWord word = dict.getWord(wordID);
            ISynset synset = word.getSynset();
            String LexFileName = synset.getLexicalFile().getName();
            // System.out.println("Lexical Name : "+ LexFileName);
            //  System.out.println("Id = " + wordID);
            //   System.out.println(" Lemma = " + word.getLemma());
            //   System.out.println(" Gloss = " + word.getSynset().getGloss());

            area.append("\nSynonims for " + word.getLemma());
            // iterate over words associated with the synset
            for (IWord w : synset.getWords()) {
                System.out.println("Syno " + w.getLemma());
                area.append("\nSyno " + w.getLemma());

            }

            area.append("\n");

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //construct the Dictionary object and open it


        // look up first sense of the word "dog "


    }

    public void askSynoNoun(String wordd, StatDetail st) {
	 	/*String wordNetDirectory = "WordNet-3.0";
        String path = wordNetDirectory + File.separator + "dict";*/
        URL url;
        IDictionary dict = null;
        try {
            url = new URL("file", null, "C:\\Users\\Hasaka\\workspace\\AES\\dict");
            dict = new Dictionary(url);
            try {
                dict.open();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            IIndexWord idxWord = dict.getIndexWord(wordd, POS.NOUN);
            IWordID wordID = idxWord.getWordIDs().get(0);
            IWord word = dict.getWord(wordID);
            ISynset synset = word.getSynset();
            String LexFileName = synset.getLexicalFile().getName();
            System.out.println("Syn for " + word.getLemma());
            // iterate over words associated with the synset
            for (IWord w : synset.getWords()) {
                //System .out . println ("Synoooosss "+w. getLemma ());
                for (int i = 0; i < st.count; i++) {
                    if (st.parts[i].equals(w.getLemma())) {
                        System.out.println("Synonim matched ");
//						 ProblemChecking.probCnt+=1;
                    }
                }
            }
            //System.out.println(st.parts[1]);
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //construct the Dictionary object and open it


        // look up first sense of the word "dog "


    }

    public void askSynoVerb(String wordd, JTextArea area) {
	 	/*String wordNetDirectory = "WordNet-3.0";
        String path = wordNetDirectory + File.separator + "dict";*/
        URL url;
        IDictionary dict = null;

        try {
            url = new URL("file", null, "C:\\Users\\Hasaka\\workspace\\AES\\dict");
            dict = new Dictionary(url);
            try {
                dict.open();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            IIndexWord idxWord = dict.getIndexWord(wordd, POS.VERB);
            IWordID wordID = idxWord.getWordIDs().get(0);
            IWord word = dict.getWord(wordID);
            ISynset synset = word.getSynset();
            String LexFileName = synset.getLexicalFile().getName();
	       /* System.out.println("Lexical Name : "+ LexFileName);
	        System.out.println("Id = " + wordID);
	        System.out.println(" Lemma = " + word.getLemma());
	        System.out.println(" Gloss = " + word.getSynset().getGloss());*/

            area.append("\nSynonims for " + word.getLemma());
            // iterate over words associated with the synset
            for (IWord w : synset.getWords()) {
                System.out.println("Syno " + w.getLemma());
                area.append("\nSyno " + w.getLemma());

            }
            area.append("\n");


        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void chkSyno(String wordd, JTextArea area) {
        URL url;
        IDictionary dict = null;
        //ProblemChecking chk =new ProblemChecking();
        try {
            url = new URL("file", null, "C:\\Users\\Hasaka\\workspace\\AES\\dict");
            dict = new Dictionary(url);
            try {
                dict.open();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            IIndexWord idxWord = dict.getIndexWord(wordd, POS.NOUN);
            IWordID wordID = idxWord.getWordIDs().get(0);
            IWord word = dict.getWord(wordID);
            ISynset synset = word.getSynset();
            // iterate over words associated with the synset

            for (IWord w : synset.getWords()) {
                System.out.println("Syno " + w.getLemma());
                //area.append("\nSyno "+w. getLemma ());
                // chk.ProblemRelateWords(w.getLemma());
            }

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


}
