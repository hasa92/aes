package AES;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class GenerateExcel {
	
	private static String FILE = "FirstPdf.pdf";
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
                    Font.BOLD);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
                    Font.NORMAL, BaseColor.RED);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
                    Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
                    Font.BOLD);
    private static Font smallfont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.NORMAL);
    private static Font smallfontGreen = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.NORMAL, BaseColor.GREEN);

	public void excel(String prob,String sol,String meth,String res){
		try {
            String filename = "NewExcelFile.xls" ;
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("FirstSheet");  

            HSSFRow rowhead = sheet.createRow((short)0);
            rowhead.createCell(0).setCellValue("Problem agent");
            rowhead.createCell(1).setCellValue("Solution agent");
            rowhead.createCell(2).setCellValue("Methodology agent");
            rowhead.createCell(3).setCellValue("Result");

            HSSFRow row = sheet.createRow((short)1);
            row.createCell(0).setCellValue(prob);
            row.createCell(1).setCellValue(sol);
            row.createCell(2).setCellValue(meth);
            row.createCell(3).setCellValue(res);

            FileOutputStream fileOut = new FileOutputStream(filename);
            workbook.write(fileOut);
            fileOut.close();
            System.out.println("Your excel file has been generated!");

        } catch ( Exception ex ) {
            System.out.println(ex);
        }
	}
	
	public void genPdf(String prob,String sol,String meth,String res, String ab,String er,List<String> mw,String fm,List<String> gee){
		try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            addTitlePage(document, prob, sol, meth, res,ab,er,mw,fm,gee);
            document.close();
    } catch (Exception e) {
            e.printStackTrace();
    }
	
	}

	private void addTitlePage(Document document,String prob,String sol,String meth,String res,String ab,String er,List<String> mw,String fm,List<String> gee)throws DocumentException {
		Paragraph preface = new Paragraph();
        // We add one empty line
        addEmptyLine(preface, 1);
        // Lets write a big header
        preface.setAlignment(Element.ALIGN_CENTER);
        preface.add(new Paragraph("Abstract Evaluation System", catFont));

        addEmptyLine(preface, 3);
        
        preface.add(new Paragraph("Your abstract", subFont));

        
        preface.add(new Paragraph(ab, smallfont));
        addEmptyLine(preface, 1);
        
        preface.add(new Paragraph("Grammar errors", subFont));

        if(er==null){
        	preface.add(new Paragraph("No grammar errors", smallfont));
        }else{
        	for(String obj: gee){
            	preface.add(new Paragraph(obj, smallfont));
            }
        }
        
        
        addEmptyLine(preface, 1);
        
        preface.add(new Paragraph("Repeating words", subFont));
        addEmptyLine(preface, 1);
        

        for(String obj: mw){
        	preface.add(new Paragraph(obj, smallfont));
        }
        
        addEmptyLine(preface, 1);
        
        preface.add(new Paragraph("About research problem", subFont));

        
        preface.add(new Paragraph(prob, smallfont));
        addEmptyLine(preface, 1);
        
        preface.add(new Paragraph("About solution", subFont));

        
        preface.add(new Paragraph(sol, smallfont));
        addEmptyLine(preface, 1);
        
        preface.add(new Paragraph("About methodology", subFont));

        
        preface.add(new Paragraph(meth, smallfont));
        addEmptyLine(preface, 1);
        
        preface.add(new Paragraph("About result abstract", subFont));

        
        preface.add(new Paragraph(res, smallfont));
        addEmptyLine(preface, 1);
        
        preface.add(new Paragraph("Final marks "+fm, redFont));
        addEmptyLine(preface, 2);
        // Will create: Report generated by: _name, _date
        preface.add(new Paragraph(
                        "Report generated by: Abstract Evaluation System and generated by " + System.getProperty("user.name") + ", " + new Date(), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                        smallBold));
        addEmptyLine(preface, 1);
        preface.add(new Paragraph(
                        "This document describes result of your Absrtact",
                        redFont));

        document.add(preface);
        // Start a new page
        document.newPage();
	}
	
	private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
                paragraph.add(new Paragraph(" "));
        }
	}
}
